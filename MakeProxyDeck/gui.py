
# GUI作成のためのライブラリ
import tkinter as tk
from tkinter import messagebox
from tkinter import scrolledtext
from tkinter import filedialog
from tkinter import ttk

# PDF作成のためのライブラリ
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm

import sys # 標準入出力をTextBoxにリダイレクトする
import os # ディレクトリの存在判定や作成
import csv # csvの読み書き
from PIL import Image # 画像の読み込み
import glob # フォルダ内の画像パス取得
import re # 正規表現によるHTMLの探索
import requests # URLからHTML取得
from bs4 import BeautifulSoup # HTMLを操作
from io import BytesIO # よくわからん(Byte型から何かへ変換．PILで読める型にする)
import threading as th # 非同期処理


class Application(ttk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.init_widgets()
        self.set_widgets()
        self.pack(expand=1, fill=tk.BOTH, anchor=tk.NW)
        sys.stdout = self.StdoutRedirector(self.text)
        sys.stderr = self.StderrRedirector(self.text)

        self.out_dir = "./デッキpdf"
        os.makedirs(self.out_dir, exist_ok=True)
        self.img_dir = "./デッキ画像"
        os.makedirs(self.img_dir, exist_ok=True)

        self.ROOT_URL = "https://www.pokemon-card.com"

        self.im_width = 63 * mm
        self.im_height = 88 * mm
        self.pdf_margin = 10 * mm
        self.im_place = [(self.im_width*j + self.pdf_margin, self.im_height*i + self.pdf_margin) 
                        for i in range(3) for j in range(3)]

        self.rewrite_ok = False

    def init_widgets(self):
        # 定義
        # <input frame>
        self.input_frame = ttk.Frame(self)

        self.deck_label = ttk.Label(self.input_frame, text='デッキ名')
        self.var_deck_entry = tk.StringVar()
        self.deck_entry = ttk.Entry(self.input_frame, textvariable = self.var_deck_entry)

        self.url_label = ttk.Label(self.input_frame, text='URL')
        self.var_url_entry = tk.StringVar()
        self.url_entry = ttk.Entry(self.input_frame, textvariable=self.var_url_entry)

        self.code_label = ttk.Label(self.input_frame, text='デッキコード')
        self.var_code_entry = tk.StringVar()
        self.code_entry = ttk.Entry(self.input_frame, textvariable=self.var_code_entry, state=tk.DISABLED)
        # </input frame>

        # <config frame>
        self.config_frame = ttk.Frame(self)

        self.do_type = tk.IntVar()
        self.do_type.set(0)
        self.from_url = ttk.Radiobutton(self.config_frame, text='URLから作成', value=0, 
                                        variable=self.do_type, command=self.change_state)
        self.from_image = ttk.Radiobutton(self.config_frame, text='画像から作成', value=1, 
                                        variable=self.do_type, command=self.change_state)
        self.from_code = ttk.Radiobutton(self.config_frame, text='デッキコードから作成', value=2, 
                                        variable=self.do_type, command=self.change_state)

        self.explore_button = ttk.Button(self.config_frame, text="フォルダを選択", 
                                        command=self.explore_folder, state="disabled")
        self.pdf_button = ttk.Button(self.config_frame, text='PDFを作成', command=self.check_path)
        # </config frame>
        
        # <output frame>
        self.output_frame = ttk.Frame(self)

        self.text = tk.scrolledtext.ScrolledText(self.output_frame, state=tk.DISABLED)
        self.var_pb = tk.IntVar()
        self.var_pb.set(0)
        self.pb = ttk.Progressbar(self.output_frame, mode="determinate", variable=self.var_pb)
        # </output frame>

    def set_widgets(self):
        # 配置
        # <input frame>
        self.deck_label.grid(row=0, column=0, sticky=tk.W, padx=5, pady=5)
        self.deck_entry.grid(row=0, column=1, sticky=tk.EW, padx=5, pady=5)
        self.deck_entry.focus_set()
        
        self.url_label.grid(row=1, column=0, sticky=tk.W, padx=5, pady=5)
        self.url_entry.grid(row=1, column=1, sticky=tk.EW, padx=5, pady=5)

        self.code_label.grid(row=2, column=0, sticky=tk.W, padx=5, pady=5)
        self.code_entry.grid(row=2, column=1, sticky=tk.EW, padx=5, pady=5)

        self.input_frame.grid(row=0, column=0, sticky=tk.EW) # input frame 配置
        # </input frame>

        # <config frame>
        self.from_url.grid(row=0, column=0, sticky=tk.W, padx=5, pady=5)
        self.from_image.grid(row=0, column=1, sticky=tk.W, padx=5, pady=5)
        self.from_code.grid(row=0, column=2, sticky=tk.W, padx=5, pady=5)

        self.explore_button.grid(row=0, column=3, sticky=tk.E, padx=5, pady=5)
        self.explore_button.bind("<Return>", self.explore_folder)
        self.pdf_button.grid(row=0, column=4, sticky=tk.E, padx=5, pady=5)
        self.pdf_button.bind("<Return>", self.check_path)

        self.config_frame.grid(row=1, column=0, sticky=tk.EW) # config frame 配置
        # </config frame>

        # <output frame>
        self.text.grid(row=0, column=0, sticky=tk.NSEW, padx=5, pady=5)
        self.pb.grid(row=1, column=0, sticky=tk.EW, padx=5, pady=5)

        self.output_frame.grid(row=2, column=0, sticky=tk.NSEW) # output frame 配置
        # </output frame>

        self.input_frame.columnconfigure(1, weight=1)
        self.config_frame.rowconfigure(0, weight=1)
        self.config_frame.columnconfigure(3, weight=1)
        self.output_frame.rowconfigure(0, weight=1)
        self.output_frame.columnconfigure(0, weight=1)
        self.rowconfigure(2, weight=1)
        self.columnconfigure(0, weight=1)

    def my_insert(self, msg, end="\n"):
        # テキストボックスに書き込むときだけ書き込めるようにする
        self.text.configure(state = tk.NORMAL)
        self.text.insert("end", msg + end)
        self.text.configure(state = tk.DISABLED)

    def change_state(self):
        # ラジオボタンと入力ボックスの対応
        if self.do_type.get() == 0:
            self.url_entry.configure(state=tk.NORMAL)
            self.code_entry.configure(state=tk.DISABLED)
            self.explore_button.configure(state=tk.DISABLED)
        elif self.do_type.get() == 1:
            self.url_entry.configure(state=tk.DISABLED)
            self.code_entry.configure(state=tk.DISABLED)
            self.explore_button.configure(state=tk.NORMAL)
        else:
            self.url_entry.configure(state=tk.DISABLED)
            self.code_entry.configure(state=tk.NORMAL)
            self.explore_button.configure(state=tk.DISABLED)
    
    def explore_folder(self):
        # デッキ名を入力しなくてもフォルダを選択して作成可
        fld = filedialog.askdirectory(initialdir=self.img_dir)
        if type(fld) is str:
            self.var_deck_entry.set(fld.split("/")[-1])

    def check_path(self, *arg):
        # デッキ名は必ず必要
        if self.deck_entry.get() is None:
            messagebox.showerror("エラー！", "デッキ名を入力してください．")

        # URLから作成するときのエラーチェック
        if self.do_type.get() == 0:
            if os.path.exists(self.out_dir + "/" + self.var_deck_entry.get() + ".pdf"):
                self.rewrite_ok = messagebox.askyesno("注意！", "そのデッキは既に存在しています．上書きしても良いですか？")
                if self.rewrite_ok:
                    th.Thread(target=self.make_from_url).start()
                else:
                    self.my_insert("最初からやり直してください．")
                    return
            elif not "https://www.pokemon-card.com/deck/result.html/deckID/" in self.url_entry.get():
                messagebox.showerror("エラー！", "正しいURLを入力してください．")
            elif not self.url_entry.get()[-1] == "/":
                messagebox.showerror("エラー！", "保存完了したデッキのURLを入力してください．")
            else:
                th.Thread(target=self.make_from_url).start()

        # 画像から作成するときのエラーチェック
        if self.do_type.get() == 1:
            if os.path.exists(self.out_dir + "/" + self.var_deck_entry.get() + ".pdf"):
                self.rewrite_ok = messagebox.askyesno("注意！", "そのデッキは既に存在しています．上書きしても良いですか？")
                if self.rewrite_ok:
                    th.Thread(target=self.make_from_image).start()
                else:
                    self.my_insert("最初からやり直してください．")
                    return
            elif not os.path.exists(self.img_dir + "/" + self.deck_entry.get()):
                messagebox.showerror("エラー！", "デッキは「デッキ画像」フォルダ内に配置してください．")
            else:
                th.Thread(target=self.make_from_image).start()

        # デッキコードから作成するときのエラーチェック
        if self.do_type.get() == 2:
            if not len(self.var_code_entry.get()) == 20:
                messagebox.showerror("エラー！", "デッキコードの長さが違います．正しいデッキコードを入力してください．\n例) ABCdEf-hIJKLM-noPQRs")
            else:
                th.Thread(target=self.make_from_url).start()

    def make_from_image(self):
        all_cards = glob.glob(self.img_dir + "/" + self.deck_entry.get() + '/*.jpg')
        all_cards_num = len(all_cards)
        if all_cards_num == 0:
            messagebox.showerror("エラー！", "フォルダ内にカードがありません．")
            self.my_insert("最初からやり直してください．")
            return
        
        self.my_insert("pdfを作成中...")

        pdf = canvas.Canvas(self.out_dir + "/" + self.deck_entry.get() + '.pdf', pagesize=A4)

        self.pb.configure(maximum=all_cards_num)

        for index, im_path in enumerate(all_cards):
            im = Image.open(im_path)
            pdf.drawInlineImage(im, self.im_place[index % 9][0], self.im_place[index % 9][1], 
                                width=self.im_width, height=self.im_height)
            if (index + 1) % 9 == 0:
                pdf.showPage()
            self.var_pb.set(index + 1)

        try:
            pdf.save()
        except Exception as ex:
            messagebox.showerror("エラー！", str(ex) + "\npdfファイルを閉じてから再実行してください．")
            self.my_insert("最初からやり直してください．")
            self.var_pb.set(0)
            return
        self.my_insert("pdfの作成完了！\n")

    def make_from_url(self):
        #読み込んだHTMLからカードの枚数に該当する場所を検索
        #その後余計なアンダーバーを削除
        def get_card_num(attr_id):
            attr_str = soup.find_all(id = attr_id)[0].attrs["value"]

            if not attr_id == "copyDeckID":
                return [int(num.strip("_")) for num in re.findall("_[0-9]{1,2}_", attr_str)]

            else:
                return attr_str

        def get_card_img_paths():
            #画像のパスを取得
            card_img_names = soup.find_all("script", class_ = False, text = re.compile("PCGDECK"), 
                                            id = False, src = False, type = False)[0].contents[0]
            return re.findall("/assets/images/card_images/large/.+/.+jpg", card_img_names)
                # list(dict.fromkeys(re.findall("[ぁ-んァ-ン一-龥ー・]+(GX|\b)", card_img_names)))

        if not os.path.exists(self.out_dir + "/デッキコード.csv"):
            with open(self.out_dir + "/デッキコード.csv", "w") as f:
                writer = csv.writer(f, lineterminator = "\n")
                writer.writerow(["デッキ名", "デッキコード"])

        self.my_insert("HTMLを解析中...")
        if self.do_type.get() == 0:
            rq = requests.get(self.url_entry.get())
        elif self.do_type.get() == 2:
            rq = requests.get("https://www.pokemon-card.com/deck/result.html/deckID/" + self.code_entry.get() + "/")

        #rq.text(string型)は文字化けする可能性あり．
        #rq.content(byte型)を渡せば自動で文字コードを判断．（らしい）
        soup = BeautifulSoup(rq.content, "lxml")

        #カードの種類とID(ポケモン，グッズ，サポート，スタジアム，エネルギー，ID)
        ATTRS = ["deck_pke", "deck_gds", "deck_sup", "deck_sta", "deck_ene", "copyDeckID"]

        card_each_num = {}

        self.my_insert("カードの枚数を取得中...")
        for attr in ATTRS:
            card_each_num[attr] = get_card_num(attr)

        self.my_insert("カードの画像パスを取得中...")
        card_img_paths = get_card_img_paths()

        if len(card_img_paths) == 0:
            messagebox.showerror("エラー！", "カードの画像を取得できませんでした．")
            self.my_insert("最初からやり直してください．")
            return

        self.my_insert("HTMLの解析完了！")

        with open(self.out_dir + "/" + "デッキコード.csv", "a") as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow([self.deck_entry.get(), card_each_num["copyDeckID"]])
        self.my_insert("デッキコードを保存しました．")

        self.my_insert("pdfを作成中...")
        pdf = canvas.Canvas(self.out_dir + "/" + self.deck_entry.get() + '.pdf', pagesize=A4)

        self.pb.configure(maximum=60)

        index = 0
        path_idx = -1
        for attr in ATTRS:
            if attr == "copyDeckID":
                continue
            for card_num in card_each_num[attr]:
                path_idx += 1
                try:
                    rq = requests.get(self.ROOT_URL + card_img_paths[path_idx])
                    im = Image.open(BytesIO(rq.content))
                except Exception as ex:
                    messagebox.showerror("エラー！", str(ex) + "\nGIF画像を使っている可能性があります．\nJPG画像を選択してください．")
                    self.var_pb.set(0)
                    self.my_insert("最初からやり直してください．")
                    return
                for _ in range(card_num):
                    pdf.drawInlineImage(im, self.im_place[index % 9][0], self.im_place[index % 9][1], 
                                        width=self.im_width, height=self.im_height)
                    if (index + 1) % 9 == 0:
                        pdf.showPage()
                    index += 1
                    self.var_pb.set(index)
                        
        try:
            pdf.save()
        except Exception as ex:
            messagebox.showerror("エラー！", str(ex) + "\npdfファイルを閉じてから再実行してください．")
            self.my_insert("最初からやり直してください．")
            self.var_pb.set(0)
            return
        self.my_insert("pdfの作成完了！\n")
        return


    class IORedirector(object):
        def __init__(self, text_area):
            self.text_area = text_area

    class StdoutRedirector(IORedirector):
        def write(self, st):
            self.text_area.insert(tk.INSERT, st)

    class StderrRedirector(IORedirector):
        def write(self, st):
            self.text_area.insert(tk.INSERT, st)


root = tk.Tk()
root.minsize(550, 400)
root.title("プロキシカード作成")
app = Application(master=root)
app.mainloop()