from PIL import Image
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
import glob
import os

def make_from_image(deck_name = ""):
    imWidth = 63 * mm
    imHeight = 88 * mm
    margin = 10 * mm

    # while True:
    #     deckName = input("カードが入っているフォルダの名前を入力してください．:")
    #     if os.path.exists('./' + deckName):
    #         break
    #     else:
    #         print("直下に存在するフォルダ名を入力してください．")

    print("pdf作成中...")
    pdf = canvas.Canvas('./' + deck_name + "/" + deck_name + '.pdf', pagesize=A4)

    imPlace = [(imWidth*j + margin, imHeight*i + margin) for i in range(3) for j in range(3)]

    all_cards = glob.glob('./' + deck_name + '/*.jpg')
    all_cards_num = len(all_cards)

    for index, imPath in enumerate(all_cards):
        im = Image.open(imPath)
        pdf.drawInlineImage(im, imPlace[index % 9][0], imPlace[index % 9][1], width=imWidth, height=imHeight)
        if (index + 1) % 9 == 0:
            pdf.showPage()
        print("\r{0:2d}/{1:2d}".format(index + 1, all_cards_num), end = "")

    pdf.save()
    print()
    print("pdf作成完了！")
