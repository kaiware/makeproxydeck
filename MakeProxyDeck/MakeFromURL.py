import re
import requests
from PIL import Image
from bs4 import BeautifulSoup
from io import BytesIO
import csv

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
import os

def make_from_url(url = "", deck_name = ""):
    #読み込んだHTMLからカードの枚数に該当する場所を検索
    #その後余計なアンダーバーを削除
    def get_card_num(attr_id):
        attr_str = soup.find_all(id = attr_id)[0].attrs["value"]

        if not attr_id == "copyDeckID":
            return [int(num.strip("_")) for num in re.findall("_[0-9]{1,2}_", attr_str)]

        else:
            return attr_str

    def get_card_img_paths():
        #画像のパスを取得
        card_img_names = soup.find_all("script", class_ = False, text = re.compile("PCGDECK"), 
                                        id = False, src = False, type = False)[0].contents[0]
        return re.findall("/assets/images/card_images/large/.+/.+jpg", card_img_names)
            # list(dict.fromkeys(re.findall("[ぁ-んァ-ン一-龥ー・]+(GX|\b)", card_img_names)))

    ROOT_URL = "https://www.pokemon-card.com"

    # os.makedirs("./デッキfromURL", exist_ok=True)
    if not os.path.exists("./デッキfromURL/デッキコード.csv"):
        with open("./デッキfromURL/デッキコード.csv", "w") as f:
            writer = csv.writer(f, lineterminator = "\n")
            writer.writerow(["デッキ名", "デッキコード"])

    # while True:
    #     url = input("デッキのURLを入力してください : ")
    #     if not "https://www.pokemon-card.com/deck/result.html/deckID/" in url:
    #         print("保存完了したデッキのURLを入力してください．")
    #     elif not url[-1] == "/":
    #         print("正しいURLを入力してください．")
    #     else:
    #         break

    print("HTMLを解析中...")
    rq = requests.get(url)

    #rq.text(string型)は文字化けする可能性あり．
    #rq.content(byte型)を渡せば自動で文字コードを判断．
    soup = BeautifulSoup(rq.content, "lxml")

    #カードの種類とID(ポケモン，グッズ，サポート，スタジアム，エネルギー，ID)
    ATTRS = ["deck_pke", "deck_gds", "deck_sup", "deck_sta", "deck_ene", "copyDeckID"]

    card_each_num = {}

    print("カードの枚数を取得中...")
    for attr in ATTRS:
        card_each_num[attr] = get_card_num(attr)

    print("カードの画像パスを取得中...")
    card_img_paths = get_card_img_paths()

    print("HTMLの解析完了！")


    im_width = 63 * mm
    im_height = 88 * mm
    margin = 10 * mm

    # while True:
    #     deck_name = input("デッキ名を付けて下さい．: ")
    #     if os.path.exists("./デッキfromURL/" + deck_name + ".pdf"):
    #         print("そのデッキはすでに存在しています．別の名前を付けて下さい．")
    #     else:
    #         break

    with open("./デッキfromURL/" + "デッキコード.csv", "a") as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerow([deck_name, card_each_num["copyDeckID"]])
    print("デッキコードを保存しました．")

    print("pdfを作成中...")
    pdf = canvas.Canvas("./デッキfromURL/" + deck_name + '.pdf', pagesize=A4)

    im_place = [(im_width*j + margin, im_height*i + margin) for i in range(3) for j in range(3)]

    index = 0
    path_idx = -1
    for attr in ATTRS:
        if attr == "copyDeckID":
            continue
        for card_num in card_each_num[attr]:
            path_idx += 1
            rq = requests.get(ROOT_URL + card_img_paths[path_idx])
            im = Image.open(BytesIO(rq.content))
            for _ in range(card_num):
                pdf.drawInlineImage(im, im_place[index % 9][0], im_place[index % 9][1], width=im_width, height=im_height)
                if (index + 1) % 9 == 0:
                    pdf.showPage()
                index += 1
                print("\r{0:2d}/60".format(index), end="")

    pdf.save()
    print()
    print("pdfの作成完了！")
