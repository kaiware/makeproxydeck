 
import tkinter as tk
import tkinter.ttk as ttk
 
 
class MainFrame(ttk.Frame):
    def __init__(self, master=None, **kwargs):
        super().__init__(master, **kwargs)
        self.create_widgets()
 
    def create_widgets(self):
        """ウィジェットの作成と配置"""
        # 左側、言語リストの作成
        self.languages = tk.StringVar(value=('Python', 'C', 'Tcl/Tk'))
        self.left = tk.Listbox(self, listvariable=self.languages)
 
        # 右側、テキストエリアの作成
        self.right = tk.Text(self)
 
        # ウィジェットの配置
        self.left.grid(column=0, row=0, sticky=(tk.N, tk.S, tk.E, tk.W))
        self.right.grid(column=1, row=0, sticky=(tk.N, tk.S, tk.E, tk.W))
 
        # ウィジェットの引き伸ばし設定
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)
 
 
root = tk.Tk()
app = MainFrame(root)
app.grid(column=0, row=0, sticky=(tk.N, tk.S, tk.E, tk.W))
# root.columnconfigure(0, weight=1)
# root.rowconfigure(0, weight=1)
root.mainloop()
 